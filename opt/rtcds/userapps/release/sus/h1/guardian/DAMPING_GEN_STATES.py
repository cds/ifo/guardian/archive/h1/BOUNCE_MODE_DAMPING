import cdsutils
import numpy
import time

from guardian import GuardState


def zero_damping_gains(mode):
    if mode == 'ROLL':
        filter_name_list = ['SUS-ETMY_M0_DARM_DAMP_R', 'SUS-ETMX_M0_DARM_DAMP_R', 'SUS-ITMY_M0_DARM_DAMP_R', 'SUS-ITMX_M0_DARM_DAMP_R']
    elif mode == 'BOUNCE':
        filter_name_list = ['SUS-ETMY_M0_DARM_DAMP_V', 'SUS-ETMX_M0_DARM_DAMP_V', 'SUS-ITMY_M0_DARM_DAMP_V', 'SUS-ITMX_M0_DARM_DAMP_V']
    else:
        raise ValueError('Undefined mode: {}'.format(mode))

    for filter_name in filter_name_list:
        ezca.get_LIGOFilter(filter_name).GAIN.put(0)


def reset_damping_monitors(mode):
    if mode == 'ROLL':
        monitor_channel_name_list = ['OAF-ROLL_ALL_RSET', 'OAF-ROLL_ETMX_RSET', 'OAF-ROLL_ETMY_RSET', 'OAF-ROLL_ITMY_RSET', 'OAF-ROLL_ITMX_RSET']
    elif mode == 'BOUNCE':
        monitor_channel_name_list = ['OAF-BOUNCE_ALL_RSET', 'OAF-BOUNCE_ETMX_RSET', 'OAF-BOUNCE_ETMY_RSET', 'OAF-BOUNCE_ITMY_RSET', 'OAF-BOUNCE_ITMX_RSET']
    else:
        raise ValueError('Undefined mode: {}'.format(mode))

    for channel_name in monitor_channel_name_list:
        ezca[channel_name] = 2


def get_all_monitor_abs_max(mode):
    if mode == 'ROLL':
        monitor_channel_name_list = ['OAF-ROLL_ETMY_RMS_OUTMON', 'OAF-ROLL_ETMX_RMS_OUTMON', 'OAF-ROLL_ITMY_RMS_OUTMON', 'OAF-ROLL_ITMX_RMS_OUTMON']
    elif mode == 'BOUNCE':
        monitor_channel_name_list = ['OAF-BOUNCE_ETMY_RMS_OUTMON', 'OAF-BOUNCE_ETMX_RMS_OUTMON', 'OAF-BOUNCE_ITMY_RMS_OUTMON', 'OAF-BOUNCE_ITMX_RMS_OUTMON']
    else:
        raise ValueError('Undefined mode: {}'.format(mode))

    cds_data_object_list = cdsutils.getdata(monitor_channel_name_list, 1)

    all_monitor_abs_max = 0
    for cds_data_object in cds_data_object_list:
        monitor_abs_max = numpy.max(numpy.abs(cds_data_object.data))
        if monitor_abs_max > all_monitor_abs_max:
            all_monitor_abs_max = monitor_abs_max

    return all_monitor_abs_max


def gen_OFF(mode):
    class OFF(GuardState):
        goto = True
        def main(self):
            zero_damping_gains(mode)
            return True

    return OFF


def gen_RESET_DAMPING_MONITORS(mode):
    class RESET_DAMPING_MONITORS(GuardState):
        def main(self):
            try:
                reset_damping_monitors(mode)
            except ValueError as e:
                log(e.strerror)
                return 'FAULT'

            return True

    return RESET_DAMPING_MONITORS


def gen_SET_DAMPING_GAINS(mode):
    class SET_DAMPING_GAINS(GuardState):
        def main(self):
            time.sleep(60)

            if mode == 'ROLL':
                self.monitor_channel_name_list = ['OAF-ROLL_ETMY_RMS_OUTMON', 'OAF-ROLL_ETMX_RMS_OUTMON', 'OAF-ROLL_ITMY_RMS_OUTMON', 'OAF-ROLL_ITMX_RMS_OUTMON']
                self.filter_name_list = ['SUS-ETMY_M0_DARM_DAMP_R', 'SUS-ETMX_M0_DARM_DAMP_R', 'SUS-ITMY_M0_DARM_DAMP_R', 'SUS-ITMX_M0_DARM_DAMP_R']
                self.target_counts = 200.0
                self.alpha = 255.0
                self.max_gain = 30.0
                self.dead_band_percent = 0.1
                self.all_monitor_abs_max_lower_bound = 0.35
            elif mode == 'BOUNCE':
                self.monitor_channel_name_list = ['OAF-BOUNCE_ETMY_RMS_OUTMON', 'OAF-BOUNCE_ETMX_RMS_OUTMON', 'OAF-BOUNCE_ITMY_RMS_OUTMON', 'OAF-BOUNCE_ITMX_RMS_OUTMON']
                self.filter_name_list = ['SUS-ETMY_M0_DARM_DAMP_V', 'SUS-ETMX_M0_DARM_DAMP_V', 'SUS-ITMY_M0_DARM_DAMP_V', 'SUS-ITMX_M0_DARM_DAMP_V']
                self.target_counts = 200.0
                self.alpha = 500.0
                self.max_gain = 2.0
                self.dead_band_percent = 0.1
                self.all_monitor_abs_max_lower_bound = 0.35
            else:
                log('Undefined mode: {}'.format(mode))
                return 'FAULT'

            for filter_name in self.filter_name_list:
                ezca.get_LIGOFilter(filter_name).TRAMP.put(10.0)

            self.previous_set_gain = 0
            self.damped = False

        def run(self):
            try:
                self.all_monitor_abs_max = get_all_monitor_abs_max(mode)
            except ValueError as e:
                log(e.strerror)
                return 'FAULT'

            log('ALL MONITOR ABS MAX: {}'.format(self.all_monitor_abs_max))

            if self.all_monitor_abs_max <= self.all_monitor_abs_max_lower_bound:
                self.damped = True

            if self.damped == False:
                self.set_gain = self.target_counts * self.alpha / self.all_monitor_abs_max
                self.set_gain = round(self.set_gain, 4)

                log('GAIN: {}'.format(self.set_gain))

                if self.set_gain > self.max_gain:
                    self.set_gain = self.max_gain

                if self.set_gain < 0:
                    self.set_gain = 0

                if abs(self.set_gain - self.previous_set_gain) > abs(self.previous_set_gain * self.dead_band_percent):
                    for filter_name in self.filter_name_list:
                        ezca.get_LIGOFilter(filter_name).GAIN.put(self.set_gain)

                    self.previous_set_gain = self.set_gain

            time.sleep(1)
            return True

    return SET_DAMPING_GAINS


def gen_FAULT():
    class FAULT(GuardState):
        def main(self):
            return True

    return FAULT

